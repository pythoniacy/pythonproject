import math
from word import Word, WordList
from preprocessedfile import PreprocessedFile


class Cluster:

    _similarity_matrix = None

    @staticmethod
    def prepare_similarity_matrix(clusters):
        print("preparing similarity matrix step 1...")
        Cluster._similarity_matrix = []
        for i in range(len(clusters)):
            Cluster._similarity_matrix.append([])
            for j in range(len(clusters)):
                Cluster._similarity_matrix[i].append(0)
        print("preparing similarity matrix step 2...")
        for i in range(len(clusters)):
            for j in range(i + 1, len(clusters)):
                Cluster._similarity_matrix[i][j] = clusters[i].get_similarity_2(clusters[j])

        print("preparing similarity matrix step 3...")
        for i in range(1, len(clusters)):
            for j in range(i):
                Cluster._similarity_matrix[i][j] = Cluster._similarity_matrix[j][i]
        print("similarity matrix prepared")

    @staticmethod
    def hierarchical_clustering(clusters, objective_clusters_count):
        Cluster.prepare_similarity_matrix(clusters)
        while len(clusters) > objective_clusters_count:
            _max_similarity = 0
            _merge_i = -1
            _merge_j = -1
            for i in range(len(clusters)):
                for j in range(len(clusters)):
                    if i == j:
                        continue
                    if Cluster._similarity_matrix[i][j] > _max_similarity:
                        _max_similarity = Cluster._similarity_matrix[i][j]
                        _merge_i = i
                        _merge_j = j
            if _merge_i != -1 and _merge_j != -1 and _merge_i != _merge_j:
                clusters[_merge_i].merge_clusters(clusters[_merge_j])
                clusters.remove(clusters[_merge_j])
                Cluster.prepare_similarity_matrix(clusters)
            else:
                raise RuntimeError("Can not merge clusters")

        for i in range(len(clusters)):
            print("cluster no.", i, ":");
            for file in clusters[i].preprocessed_files:
                file.print_name()


    #metoda praktycznie taka sama co "add_file"
    def merge_clusters(self, cluster):
        print("clusters merge...")
        for file in cluster.preprocessed_files:
            self.preprocessed_files.append(file)
            for word in file.word_list.value:
                self.word_list.add_or_update(word)

    def get_similarity(self, cluster):
        _result = 0
        for word in cluster.word_list.value:
            _add_value = 0
            for self_word in self.word_list.value:
                if word.value == self_word.value:
                    _add_value += self_word.count
                    break
            if _add_value > 0:
                _add_value += word.count
            _result += _add_value
        return _result

    def get_similarity_2(self, cluster):
        _result = 0
        for word in cluster.word_list.value:
            _add_value = 0
            for self_word in self.word_list.value:
                if word.value == self_word.value:
                    _add_value += self_word.count
                    break
            if _add_value > 0:
                _add_value += word.count
            if _add_value != 0:
                _result += math.pow(_add_value, 0.15)
        return _result

    def get_similarity_3(self, cluster):
        _result = 0
        for word in cluster.word_list.value:
            _add_value = 0
            for self_word in self.word_list.value:
                if word.value == self_word.value:
                    _add_value += self_word.count
                    break
            if _add_value > 0:
                _add_value += word.count
            if _add_value != 0:
                _result += math.log(_add_value, 5)
        return _result

    def add_file(self, preprocessed_file):
        self.preprocessed_files.append(preprocessed_file)
        for word in preprocessed_file.word_list.value:
            self.word_list.add_or_update(word)

    def __init__(self):
        self.preprocessed_files = []
        self.word_list = WordList()
