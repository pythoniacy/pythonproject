

class Word:
    def __init__(self, value):
        self.value = value
        self.count = 0


class WordList:
    def __init__(self):
        self.value = [];

    def add(self, word):
        self.value.append(word);

    def get_if_contains(self, word):
        _result = None;
        for single_word in self.value:
            if single_word.value == word.value:
                _result = single_word
                break
        return _result

    def if_contains(self, word):
        _result = False
        for single_word in self.value:
            if single_word.value == word.value:
                _result = True
                break
        return _result

    def add_or_update(self, word):
        _existing_word = self.get_if_contains(word)
        if _existing_word is None:
            self.add(word)
            _existing_word = word
        _existing_word.count += 1
