# -*- coding: utf-8 -*-
"""
Created on Sat May  6 16:02:27 2017

@author: pythonml
"""

import sys;

from textpreprocessor import TextPreprocessor
from filemanager import FileManager, FileInfo
from cluster import Cluster



#Main:
_text_files_dir = ""
_number_of_clusters = 2
print(sys.argv)
if len(sys.argv) > 1 and isinstance(sys.argv[1], basestring):
    _number_of_clusters = int(sys.argv[1])

if len(sys.argv) > 2 and isinstance(sys.argv[2], basestring):
    _text_files_dir = sys.argv[2]
else:
    _text_files_dir = "..\\text_files_2"

print("number of clusters: ", _number_of_clusters)
print("text files dir: ", _text_files_dir)

_files = FileManager.get_file_list(_text_files_dir)
_all_files = []
_all_files.extend(_files)
TextPreprocessor.preprocess_files(_all_files)
_file_info_objects = FileManager.get_file_info(_all_files)
_clusters = []
for file_info in _file_info_objects:
    _cluster = Cluster()
    _cluster.add_file(file_info.preprocessed_file)
    _clusters.append(_cluster)
Cluster.hierarchical_clustering(_clusters, _number_of_clusters)
#end-of-Main

