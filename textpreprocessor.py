import sys;
from word import Word, WordList;
import stemmer;
from filemanager import FileManager
import definitions

class TextPreprocessor:
    'Text preprocessing'

    @staticmethod
    def preprocess_files(file_list):
        for i in range(len(file_list)):
            #print("0: ", file_list[i][0])
            #print("1: ", file_list[i][1])
            TextPreprocessor.preprocessing(file_list[i][0], file_list[i][1], definitions.stoplist_path)

    @staticmethod
    def preprocessing(text_file_path, text_file_name, stop_list_file_path):
        print("Preprocessing: " + text_file_path + text_file_name)

        # files openinf
        _source_file = open(text_file_path + text_file_name, "r")
        _pre_processed_file = open(text_file_path + text_file_name.replace(".txt", "_post.txt"), "w")
        _stoplist_file = open(stop_list_file_path, "r")

        # words filtering usng stoplist
        _stoplist = _stoplist_file.read().splitlines()
        _source_file_content = _source_file.read().splitlines()
        _word_list = WordList()
        for single_line in _source_file_content:
            _words = single_line.split()
            _result_words = [word for word in _words if word.lower() not in _stoplist]
            for single_word in _result_words:
                single_word = single_word.replace(".", "").replace(",", "").replace(";", "").replace(":", "")
                single_word = single_word.lower()
                single_word = stemmer.stem(single_word)
                if single_word:
                    _word = Word(single_word)
                    _word_list.add_or_update(_word)
                    # _existing_word = _word_list.getIfContains(_word)
                    # if _existing_word is None:
                    #     _word_list.add(_word)
                    #     _existing_word = _word
                    # _existing_word._count += 1

        # save pre-processed file content to another file
        for single_word in _word_list.value:
            _pre_processed_file.write(single_word.value + " " + str(single_word.count) + "\n")

        # files closing
        _source_file.close()
        _pre_processed_file.close()
        _stoplist_file.close()
