import sys
import os
import glob
from preprocessedfile import PreprocessedFile

_tmp_file_list = [
    ["..\\text_files\\biology\\", "1,3,5-Trithiane"],
    ["..\\text_files\\biology\\", "1 Esdras"],
    ["..\\text_files\\biology\\", "1 Maccabees"],
    ["..\\text_files\\biology\\", "1 Peter"],
    ["..\\text_files\\biology\\", "1-aminocyclopropane-1-carboxylic acid"],
    ["..\\text_files\\biology\\", "1-Pyrroline-5-carboxylate dehydrogenase"]
]


class FileManager:

    @staticmethod
    def get_file_list(subDirectory):
        _result = []
        _files_list = [] #FileManager.get_files_list()

        _directory = sys.path[0] + "\\" + subDirectory + "\\"
        for root, directories, files in os.walk(_directory):
            for filename in files:
                #print("[Root\\Filename:]" + root + "\\" + filename)
                if "_post" not in filename:
                    _files_list.append([root, filename])
        return _files_list


    @staticmethod
    def get_file_info(fileList):
        _result = []
        for i in range(len(fileList)):
            _result.append(FileInfo(fileList[i][0], fileList[i][1]))
        return _result

    @staticmethod
    def load_all_unprocessed_files():
        _directory = sys.path[0] + "\\"
        for root, directories, files in os.walk(_directory):
            for filename in files:
                print([root + "\\", filename])
                #print("[Root:]" + root + "\\")
        return


    @staticmethod
    def load_file_info():
        return _tmp_file_list

    @staticmethod
    def get_files_list():
        return _tmp_file_list

    def __init__(self):
        return


class FileInfo:

    def __init__(self, file_path, file_name):
        self._file_path = file_path
        self._file_name = file_name
        self._preprocessed_file_name = file_name.replace(".txt", "_post.txt")
        self._file_handle = None
        #self._file_handle = open(self._file_path + self._file_name, "r") no need to open
        self.preprocessed_file = None
        self.preprocessed_file = PreprocessedFile(self._file_path, self._preprocessed_file_name)

    def __del__(self):
        if self._file_handle is not None:
            self._file_handle.close()
        # if self.preprocessed_file is not None:
        #     del self.preprocessed_file
