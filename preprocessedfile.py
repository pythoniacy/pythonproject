import sys;
from word import Word, WordList


class PreprocessedFile:

    def __init__(self, file_path, file_name):
        self._file_name = file_name
        self._file_path = file_path
        self.preprocessed_file = open(file_path + file_name, "r")
        _content = self.preprocessed_file.read().splitlines()
        self.word_list = WordList()
        for single_line in _content:
            #print(single_line)
            _line = single_line.split()
            _word = Word(_line[0])
            _word._count = int(_line[1])
            self.word_list.add(_word)
        self.preprocessed_file.close()

    def print_content(self):
        for word in self.word_list.value:
            print(word._value + " " + str(word._count))

    def print_name(self):
        print(self._file_name)

    #def __del__(self):
        #self.preprocessed_file.close()
    # def getSimilarity(self, preprocessedFile):
    #     _result = 0
    #     for word in self._word_list._value:
    #         if preprocessedFile._word_list.ifContains(word) == True:
    #             _result += 1
    #     return _result